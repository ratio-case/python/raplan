# Explanation and rationale

RaPlan approaches maintenance planning and scheduling as a probabilistic problem. This is how
failure mechanisms are often modeled in the literature. By substituting actual failure (instances)
by the expected probability thereof, the maintenance problem reduces to managing that failure
probability within acceptable bounds.

'Playing' with the relevant variables that represent the failure distributions over time, or the
planning and scheduling of timely maintenance is a very insightful exercise that can show quick
'results' or an indication of effects of different strategies. Naturally, failure impact can be of
wildly varying magnitude and stochastic models should always be audited, analyzed and interpreted by
relevant field experts.
